# -*- coding: utf-8 -*-
import os.path
import tarfile
import xlwt
import datetime

from django.views.generic import FormView, UpdateView, RedirectView, CreateView, TemplateView, ListView, DeleteView, DetailView
from django.views.generic.base import View, TemplateResponseMixin
from django.views.generic.detail import SingleObjectMixin
from django.views.generic.list import MultipleObjectMixin
from django.core.exceptions import ImproperlyConfigured, PermissionDenied
from django.conf import settings
from django.core.urlresolvers import reverse
from django.shortcuts import redirect
from django.http import Http404, HttpResponseRedirect, HttpResponse, JsonResponse
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.template.loader import render_to_string
from django.db.models import Count, Q

from registration.models import RegistrationProfile

from sabot.forms import ParticipantAddForm
from project.models import Project
from sabot.utils import get_project_details
# this is the place for generic views

class ObjectPermCheckGETMixin(object):
	def get(self, request, *args, **kwargs):
		self.object = self.get_object()
		if not self.object.has_read_permission(self.request.user) and not request.user.is_staff and not self.request.user.legacy_profile.is_orga:
			raise PermissionDenied
		#kwargs.update({'user': self.request.user})
		#print kwargs
		#kwargs['user'] = request.user
		return super(ObjectPermCheckGETMixin, self).get(request, *args, **kwargs)

	#def get_form_kwargs(self):
	#	kwargs = super(ObjectPermCheckGETMixin, self).get_form_kwargs()
	#	kwargs['user'] = self.request.user
	#	return kwargs


class ObjectPermCheckPOSTMixin(object):
	def post(self, request, *args, **kwargs):
		self.object = self.get_object()
		if not self.object.has_write_permission(self.request.user) and not request.user.is_staff and not self.request.user.legacy_profile.is_orga:
			raise PermissionDenied
		kwargs['user'] = request.user
		return super(ObjectPermCheckPOSTMixin, self).post(request, *args, **kwargs)

	#def get_form_kwargs(self):
	#	kwargs = super(ObjectPermCheckPOSTMixin, self).get_form_kwargs()
	#	kwargs['user'] = self.request.user
	#	return kwargs

class ObjectPermCheckMixin(ObjectPermCheckGETMixin, ObjectPermCheckPOSTMixin):
	pass

class CallableSuccessUrlMixin(object):
	def get_success_url(self):
		if callable(self.success_url):
			return self.success_url(self.object, self.kwargs)
		else:
			return super(CallableSuccessUrlMixin, self).get_success_url()

class ChangeNotificationMixin(object):
	def form_valid(self, form):
		changed_fields = form.changed_data
		result = super(ChangeNotificationMixin, self).form_valid(form)

		self.change_notification(changed_fields)
		return result

#		def change_notification(self, changed_fields):

class ParticipantsView(ObjectPermCheckMixin,FormView):
	connection_table_class = None
	object_class = None
	form_class = ParticipantAddForm

	def get_context_data(self, **kwargs):
		kwargs = super(ParticipantsView, self).get_context_data(**kwargs)
		if self.connection_table_class is None:
			raise ImproperlyConfigured("You have to set the connection table class")
		kwargs["participants_list"] = self.connection_table_class.objects.select_related().filter(project=self.object).order_by("user__last_name")
		kwargs["project"] = self.object
		kwargs["object"] = self.object
		kwargs["readonly"] = not self.object.has_write_permission(self.request.user) and not self.request.user.is_staff
		return kwargs

	def get_object(self):
		pk = self.kwargs.get("pk", None)
		if pk is None:
			raise ImproperlyConfigured("You have to pass a pk to ParticipantListView")
		try:
			object = self.object_class.objects.get(pk=pk)
		except self.object_class.DoesNotExist:
			raise Http404
		return object

	def form_valid(self, form):
		email = form.cleaned_data["email"]
		# look for user with this mail
		try:
			try:
				user = User.objects.get(username__iexact=email)
			except User.MultipleObjectsReturned:
				user = User.objects.annotate(profiles=Count("legacy_profile")).get(username__iexact=email,profiles=0)
			# just store the user in the connection table
			# check if this user is already connected
			res = self.connection_table_class.objects.filter(project=self.object,user=user).count()
			if res == 0:
				connection = self.connection_table_class(project=self.object,user=user)
				connection.save()

		except User.DoesNotExist:
			# we create a matching user with emailaddr=username
			site = Site.objects.get_current()

			new_user = User(username=email,email=email)
			new_user.first_name = form.cleaned_data["first_name"]
			new_user.last_name = form.cleaned_data["last_name"]
			new_user.save()
			RegistrationProfile.objects.create_profile(new_user)

			# add new user - nontheless
			connection = self.connection_table_class(project=self.object,user=new_user)
			connection.save()

			# send out custom email
			self.send_info_email(new_user)

		newForm = (self.get_form_class())()
		return self.render_to_response(self.get_context_data(form=newForm))

	def send_info_email(self, user):
		profile = RegistrationProfile.objects.get(user=user)
		site = Site.objects.get_current()

		ctx_dict = {'activation_key': profile.activation_key,
			'expiration_days': settings.ACCOUNT_ACTIVATION_DAYS,
			'install_main_url' : settings.INSTALL_MAIN_URL,
			'conference_name' : settings.CONFERENCE_NAME,
			'site': site,
			'project' : self.object,
			'user' : user}
		subject = render_to_string('registration/activation_email_autocreate_subject.txt', ctx_dict)
		# Email subject *must not* contain newlines
		subject = ''.join(subject.splitlines())

		message = render_to_string('registration/activation_email_autocreate.txt', ctx_dict)

		user.email_user(subject, message, settings.DEFAULT_FROM_EMAIL)

class ObjectBasedRedirectMixin(object):
	redirect = None
	next_view = None

	def get_redirect_url(self):
		if self.next_view is not None:
			return reverse(self.next_view)
		if self.redirect is None:
			raise ImproperlyConfigured("You have to specify either 'next_view' or 'redirect'")

		if callable(self.redirect):
			return self.redirect(self.object, self.kwargs)
		return self.redirect % self.object.__dict__


class PropertySetterView(SingleObjectMixin, ObjectBasedRedirectMixin, View):
	property_name = None
	property_value = None

	def post(self, request, *args, **kwargs):
		self.object = self.get_object()
		self.redirect_url = self.get_redirect_url()
		if callable(self.property_value):
			setattr(self.object, self.property_name, self.property_value(request, **kwargs))
		else:
			setattr(self.object, self.property_name, self.property_value)
		self.object.save()
		return HttpResponseRedirect(self.redirect_url)

class PermCheckPropertySetterView(PropertySetterView):
	permission_checker = lambda obj, user: obj.has_write_permission(user)

	def post(self, request, *args, **kwargs):
		self.object = self.get_object()
		if not self.permission_checker(self.object, self.request.user) and not self.request.user.is_staff and not self.request.user.legacy_profile.is_orga:
			raise PermissionDenied

		self.redirect_url = self.get_redirect_url()
		if callable(self.property_value):
			setattr(self.object, self.property_name, self.property_value(request, **kwargs))
		else:
			setattr(self.object, self.property_name, self.property_value)
		self.object.save()
		return HttpResponseRedirect(self.redirect_url)

class PermCheckSimpleDeleteView(SingleObjectMixin, ObjectBasedRedirectMixin, View):
	permission_checker = lambda obj, user: obj.has_write_permission(user)

	def delete(self, request, *args, **kwargs):
		self.object = self.get_object()
		if not self.permission_checker(self.object, self.request.user) and not self.request.user.is_staff and not self.request.user.legacy_profile.is_orga:
			raise PermissionDenied

		self.redirect_url = self.get_redirect_url()
		self.object.delete()
		return HttpResponseRedirect(self.redirect_url)

	def post(self, request, *args, **kwargs):
		return self.delete(request, *args, **kwargs)



class PermCheckDeleteView(DeleteView, ObjectBasedRedirectMixin):
	permission_checker = staticmethod(lambda obj, user: obj.has_write_permission(user))

	def delete(self, request, *args, **kwargs):
		self.object = self.get_object()
		if not self.permission_checker(self.object, self.request.user) and not self.request.user.is_staff and not self.request.user.legacy_profile.is_orga:
			raise PermissionDenied

		self.redirect_url = self.get_redirect_url()
		self.object.delete()
		return HttpResponseRedirect(self.redirect_url)

	def get(self, request, *args, **kwargs):
		self.object = self.get_object()
		if not self.permission_checker(self.object, self.request.user) and not self.request.user.is_staff and not self.request.user.legacy_profile.is_orga:
			raise PermissionDenied
		return super(PermCheckDeleteView, self).get(self, request, *args, **kwargs)


class OwnerSettingCreateView(CreateView):
	def form_valid(self, form):
		self.object = form.save(commit=False)
		self.object.owner = self.request.user
		self.object.save()
		return redirect(self.get_success_url())

class PermCheckUpdateView(ObjectPermCheckMixin,CallableSuccessUrlMixin,UpdateView):
	# make the form readonly if its only readable
	def get_form(self):
		form = super(PermCheckUpdateView, self).get_form()

		if not self.object.has_write_permission(self.request.user) and not self.request.user.is_staff and not self.request.user.legacy_profile.is_orga:
			for field in form.fields.values():
				field.widget.attrs["readonly"] = "readonly"
			if hasattr(form,"helper"):
				form.helper.inputs = []
		return form

	#def get_form_kwargs(self):
	#	kwargs = super(PermCheckUpdateView, self).get_form_kwargs()
	#	kwargs['user'] = self.request.user
	#	return kwargs

class PermCheckDetailView(ObjectPermCheckGETMixin, DetailView):
	pass


class EmailOutputView(TemplateView):
	queryset = None

	def get_queryset(self):
		if self.queryset is not None:
			queryset = self.queryset
			if callable(queryset):
				queryset = queryset(self.request, self.kwargs)
			elif hasattr(queryset, '_clone'):
				queryset = queryset._clone()
		else:
			raise ImproperlyConfigured("You have to enter a queryset")
		return queryset

	def get_context_data(self, **kwargs):
		context = super(EmailOutputView, self).get_context_data(**kwargs)
		context["emails"] = [ u.email for u in self.get_queryset() ]
		return context

	def get(self, request, *args, **kwargs):
		context = self.get_context_data(**kwargs)
		return self.render_to_response(context, content_type="text/plain")

class JSONListView(MultipleObjectMixin, View):
	jsonify_function = None
	def get(self, request, *args, **kwargs):
		self.object_list = self.get_queryset()
		result_list = map(self.jsonify_function, self.object_list)
		return JsonResponse(result_list, safe=False)

class XMLListView(ListView):
	def get(self, request, *args, **kwargs):
		self.object_list = self.get_queryset()
		allow_empty = self.get_allow_empty()
		if not allow_empty and len(self.object_list) == 0:
			raise Http404(_(u"Empty list and '%(class_name)s.allow_empty' is False.")
						% {'class_name': self.__class__.__name__})
		context = self.get_context_data(object_list=self.object_list)
		filenamexml = "Export_All_%s.xml" % (datetime.datetime.now())
		response = self.render_to_response(context,content_type="application/xml; charset=utf-8")
		response['Content-Disposition'] = 'attachment; filename=' + filenamexml
		return response

class CSVListView(ListView):
	def get(self, request, *args, **kwargs):
		self.object_list = self.get_queryset()
		allow_empty = self.get_allow_empty()
		if not allow_empty and len(self.object_list) == 0:
			raise Http404(_(u"Empty list and '%(class_name)s.allow_empty' is False.")
						% {'class_name': self.__class__.__name__})
		context = self.get_context_data(object_list=self.object_list)
		filenamecsv = "Export_All_%s.csv" % (datetime.datetime.now())
		response = self.render_to_response(context,content_type="text/csv; charset=utf-8")
		response['Content-Disposition'] = 'attachment; filename=' + filenamecsv
		return response

class CSVListViewHall(ListView):
	def get(self, request, *args, **kwargs):
		self.object_list = self.get_queryset()
		allow_empty = self.get_allow_empty()
		if not allow_empty and len(self.object_list) == 0:
			raise Http404(_(u"Empty list and '%(class_name)s.allow_empty' is False.")
						% {'class_name': self.__class__.__name__})
		context = self.get_context_data(object_list=self.object_list)
		filenamecsv = "Export_Hall_%s.csv" % (datetime.datetime.now())
		response = self.render_to_response(context,content_type="text/csv; charset=utf-8")
		response['Content-Disposition'] = 'attachment; filename=' + filenamecsv
		return response

class XLSListViewHall(ListView):
	def get(self, request, *args, **kwargs):
		self.object_list = self.get_queryset()
		allow_empty = self.get_allow_empty()
		if not allow_empty and len(self.object_list) == 0:
			raise Http404(_(u"Empty list and '%(class_name)s.allow_empty' is False.")
						% {'class_name': self.__class__.__name__})
		context = self.get_context_data(object_list=self.object_list)
		wb = xlwt.Workbook(encoding='utf-8')
		ws = wb.add_sheet('Hall')
		row_num = 0
		font_style = xlwt.XFStyle()
		font_style.font.bold = True
		columns = ['Project name', 'Project type', 'Project status', 'Booth status', 'Hall status', 'Tables req', 'Tables plan', 'Tables res', 'Benches req', 'Benches plan', 'Benches res', 'Chairs req', 'Chairs plan', 'Chairs res', 'Size req', 'Size plan', 'Power req', 'Power plan', 'Area', 'Location', 'Description', 'Additional', 'Extras Comment', 'Own', 'Safety', 'Comment', 'LastMinute', ]
		rows = Project.objects.all().values_list('projectName', 'projecttype', 'projectStatus', 'boothStatus', 'boothHallStatus', 'boothTables', 'boothTablesPlan', 'boothTablesRes', 'boothBenches', 'boothBenchesPlan', 'boothBenchesRes', 'boothChairs', 'boothChairsPlan', 'boothChairsRes', 'boothSize', 'boothSizePlan', 'boothPower', 'boothPowerPlan', 'boothArea', 'boothLocation', 'boothDescription', 'boothExtras', 'boothExtrasComment', 'boothOwn', 'boothSafety', 'boothComment', 'boothLastMinute', )
		for col_num in range(len(columns)):
			ws.write(row_num, col_num, columns[col_num], font_style)
			#ws.col(col_num).width = columns[col_num][1]
		font_style = xlwt.XFStyle()
		font_style.alignment.wrap = 1
		#rows = context['object_list']
		for row in rows:
			row_num += 1
			prjType = get_project_details(row[0], 'projecttype')
			prjStatus = get_project_details(row[0], 'projectStatus')
			bthStatus = get_project_details(row[0], 'boothStatus')
			bthHallStatus = get_project_details(row[0], 'boothHallStatus')
			bthPower = get_project_details(row[0], 'boothPower')
			bthPowerPlan = get_project_details(row[0], 'boothPowerPlan')
			bthArea = get_project_details(row[0], 'boothArea')
			bthExtras = get_project_details(row[0], 'boothExtras')
			tmplst = list(row)
			tmplst[1] = prjType
			tmplst[2] = prjStatus
			tmplst[3] = bthStatus
			tmplst[4] = bthHallStatus
			tmplst[16] = bthPower
			tmplst[17] = bthPowerPlan
			tmplst[18] = bthArea
			tmplst[21] = bthExtras
			row = tuple(tmplst)
			for col_num in range(len(row)):
				ws.write(row_num, col_num, row[col_num], font_style)
		#response = self.render_to_response(xlsexport,content_type="application/ms-excel")
		filenamexls = "Export_Hall_%s.xls" % (datetime.datetime.now())
		response = HttpResponse(content_type="application/vnd.ms-excel; charset=utf-8")
		response['Content-Disposition'] = 'attachment; filename=' + filenamexls
		wb.save(response)
		return response

class XLSListViewAll(ListView):
	def get(self, request, *args, **kwargs):
		self.object_list = self.get_queryset()
		allow_empty = self.get_allow_empty()
		if not allow_empty and len(self.object_list) == 0:
			raise Http404(_(u"Empty list and '%(class_name)s.allow_empty' is False.")
						% {'class_name': self.__class__.__name__})
		context = self.get_context_data(object_list=self.object_list)
		wb = xlwt.Workbook(encoding='utf-8')
		ws = wb.add_sheet('All')
		row_num = 0
		font_style = xlwt.XFStyle()
		font_style.font.bold = True
		columns = ['Projektname', 'Projecttype', 'Projektstatus', 'Stand Status', 'Workshop Status', 'Vortrag Status', 'Ansprechpartner Team', 'Projekt Interne Kommentare', 'Vorname', 'Nachname', 'email', 'Telefon', 'Website', 'Teilnehmertyp', 'Sprache', 'Projekt beschreibung_de', 'Projekt beschreibung_en', 'Kommentar', 'Kategorie', 'Video', 'Bereich', 'Nummer im Plan', 'Hallenplan-status', 'Standbeschreibung', 'Tische angefordert', 'Tische eingeplant', 'Tische aus Reserve', 'Bänke angefordert', 'Bänke eingeplant', 'Bänke aus Reserve', 'Stühle angefordert', 'Stühle eingeplant', 'Stühle aus Reserve', 'Größe angefordert', 'Größe geplant', 'Stromversorgung angefordert', 'Stromversorgung geplant', 'Zusätzliche Ausstattung', 'Stand Kommentare', 'Mitgebrachte Ausstattung', 'Sicherheitsvorkehrungen', 'Stand Interne Kommentare', 'Last-minute-Material', 'Workshop', 'Talk', 'Ausstellertickets', 'Ausstellertickets ausgegeben', 'Parktickets', 'Parktickets ausgegeben', 'Ticketstatus', 'Ticketausgabe', 'Ausgegeben von', 'Sponsorlevel', 'Areabranding', 'Workshop', 'Talk', 'Kommentare', 'Interne Kommentare', 'Event', 'Angelegt am',]
		rows = Project.objects.all().values_list('projectName', 'projecttype', 'projectStatus', 'boothStatus', 'workshopStatus', 'talkStatus', 'generalContact', 'internalComment', 'firstname', 'lastname', 'email', 'phone', 'homepage', 'projecttype', 'language', 'descriptionDE', 'descriptionEN', 'generalComment', 'projectArea', 'video', 'boothArea', 'boothLocation', 'boothHallStatus', 'boothDescription', 'boothTables', 'boothTablesPlan', 'boothTablesRes', 'boothBenches', 'boothBenchesPlan', 'boothBenchesRes', 'boothChairs', 'boothChairsPlan', 'boothChairsRes', 'boothSize', 'boothSizePlan', 'boothPower', 'boothPowerPlan', 'boothExtras', 'boothExtrasComment', 'boothOwn', 'boothSafety', 'boothComment', 'boothLastMinute', 'workshopComment', 'talkComment', 'serviceTickets', 'serviceTicketsGiven', 'serviceParking', 'serviceParkingGiven', 'serviceStatus', 'serviceDistribution', 'serviceDistributed', 'serviceSponsorlevel', 'serviceAreabranding', 'serviceWorkshop', 'serviceTalk', 'serviceComments', 'serviceInternalComments', 'generalEvent', 'createDate', )
		for col_num in range(len(columns)):
			if col_num == (len(columns) - 1):
				font_style.num_format_str = 'DD/MM/YYYY'
				ws.write(row_num, col_num, columns[col_num], font_style)
			else:
				font_style.num_format_str = 'General'	
				ws.write(row_num, col_num, columns[col_num], font_style)
				#ws.col(col_num).width = columns[col_num][1]
		font_style = xlwt.XFStyle()
		font_style.alignment.wrap = 1
		#rows = context['object_list']
		for row in rows:
			row_num += 1
			prjType = get_project_details(row[0], 'projecttype')
			prjStatus = get_project_details(row[0], 'projectStatus')
			bthStatus = get_project_details(row[0], 'boothStatus')
			wrkStatus = get_project_details(row[0], 'workshopStatus')
			tlkStatus = get_project_details(row[0], 'talkStatus')
			srvTalk = get_project_details(row[0], 'serviceTalk')
			srvWorkshop = get_project_details(row[0], 'serviceWorkshop')
			srvDistribution = get_project_details(row[0], 'serviceDistribution')
			srvStatus = get_project_details(row[0], 'serviceStatus')
			bthPowerPlan = get_project_details(row[0], 'boothPowerPlan')
			bthPower = get_project_details(row[0], 'boothPower')
			bthHallStatus = get_project_details(row[0], 'boothHallStatus')
			bthArea = get_project_details(row[0], 'boothArea')
			prjLanguag = get_project_details(row[0], 'language')
			prjEvent = get_project_details(row[0], 'generalEvent')
			prjArea = get_project_details(row[0], 'projectArea')
			bthExtras = get_project_details(row[0], 'boothExtras')
			tmplst = list(row)
			tmplst[1] = prjType
			tmplst[2] = prjStatus
			tmplst[3] = bthStatus
			tmplst[4] = wrkStatus
			tmplst[5] = tlkStatus
			tmplst[55] = srvTalk
			tmplst[54] = srvWorkshop
			tmplst[50] = srvDistribution
			tmplst[49] = srvStatus
			tmplst[36] = bthPowerPlan
			tmplst[35] = bthPower
			tmplst[22] = bthHallStatus
			tmplst[20] = bthArea
			tmplst[14] = prjLanguag
			tmplst[58] = prjEvent
			tmplst[18] = prjArea
			tmplst[37] = bthExtras
			row = tuple(tmplst)
			for col_num in range(len(row)):
				if col_num == (len(row) - 1):
					font_style.num_format_str = 'DD/MM/YYYY'
					ws.write(row_num, col_num, row[col_num], font_style)
				else:
					font_style.num_format_str = 'General'
					ws.write(row_num, col_num, row[col_num], font_style)
		#response = self.render_to_response(xlsexport,content_type="application/ms-excel")
		filenamexls = "Export_All_%s.xls" % (datetime.datetime.now())
		response = HttpResponse(content_type="application/vnd.ms-excel; charset=utf-8")
		response['Content-Disposition'] = 'attachment; filename=' + filenamexls
		wb.save(response)
		return response

class MultipleListView(TemplateView):
	template_params = {}

	def get_context_data(self, **kwargs):
		context = super(MultipleListView, self).get_context_data(**kwargs)
		for key, value in self.template_params.items():
			if callable(value):
				context[key] = value(self.request, self.kwargs)
			else:
				context[key] = value._clone()

		return context

class JobProcessingView(TemplateResponseMixin, View):
	success_url = None
	next_view = None
	redirect = None
	error_template_name = None

	def __init__(self, **kwargs):
		super(JobProcessingView, self).__init__(**kwargs)

		self.job_errors = []

	def get_success_url(self):
		if self.success_url is not None:
			return self.success_url
		if self.next_view is not None:
			return reverse(self.next_view)
		if self.redirect is None:
			raise ImproperlyConfigured("You have to specify either 'next_view' or 'redirect'")

		if callable(self.redirect):
			return self.redirect(self)

	def get_template_names(self):
		if self.error_template_name is not None:
			return [self.error_template_name]
		else:
			return super(JobProcessingView, self).get_template_names()

	def get_context_data(self, **kwargs):
		return {
			'params': kwargs,
			'errors': self.job_errors
		}

	def post(self, request, *args, **kwargs):
		if self.process_job():
			return HttpResponseRedirect(self.get_success_url())
		else: # render error template
			context = self.get_context_data(**kwargs)
			return self.render_to_response(context)


class ArchiveCreatorView(View):
	filename = None
	filelist = None


	def get_filename(self):
		return self.filename

	def process_files(self,tarobj):
		if self.filelist is not None:
			if callable(self.filelist):
				files = self.filelist(self.request, self.kwargs)
			if isinstance(self.filelist, (list,tuple)):
				files = self.filelist

			for f in files:
				if isinstance(f, tuple):
					tarobj.add(f[0],f[1])
				else:
					tarobj.add(f)


	def get(self, request, *args, **kwargs):
		response = HttpResponse(content_type="application/x-gtar")
		filename = self.get_filename()
		response["Content-Disposition"] = "attachment; filename=" + filename

		self.tarobj = tarfile.open(fileobj=response, mode='w|bz2')

		self.process_files(self.tarobj)

		self.tarobj.close()
		return response


class ObjectFileDownloader(SingleObjectMixin, View):
	upload_field = None
	content_type = None

	def get(self, request, *args, **kwargs):
		self.object = self.get_object()
		try:
			upload_field = getattr(self.object, self.upload_field)
		except AttributeError:
			raise ImproperlyConfigured("The upload field with name {} does not exist".format(self.upload_field))
		response = HttpResponse(upload_field.file, content_type=self.content_type)
		response["Content-Disposition"] = "attachment; filename=" + os.path.basename(upload_field.name)
		return response
