import os.path
import random
import string
from django.contrib.auth.models import User
from django.conf import settings
import logging
from django.db import connections
from django.utils.crypto import get_random_string
import requests
from project.models import Project

logger = logging.getLogger('djangosaml2')

def random_filename_generator(size=16, chars=string.ascii_lowercase + string.digits):
	return ''.join(random.choice(chars) for x in range(size))

def random_filename_upload(basedir):
	upload_transform_func(basedir)
	return upload_transform_func

def upload_transform_func(filename):
	fn, ext = os.path.splitext(filename)
	while True:
			newname = random_filename_generator() + ext
			path = os.path.join(filename, newname)
			if not os.path.isfile(os.path.join(settings.MEDIA_ROOT, path)):
					break
	return path

def get_user_details(user, detail):
	#userRaw = User.objects.raw('SELECT * FROM auth_user WHERE username = %s LIMIT 1', [user])
	userRaw = User.objects.filter(username=user)
	if (detail == 'firstname'):
			userDetail = userRaw[0].first_name
	elif (detail == 'lastname'):
			userDetail = userRaw[0].last_name
	elif (detail == 'email'):
			userDetail = userRaw[0].email
	elif (detail == 'isstaff'):
			userDetail = userRaw[0].is_staff
        elif (detail == 'id'):
                        userDetail = userRaw[0].id

	return userDetail

def get_project_details(id, detail):
	prjID = get_project_id(id)
	projectRaw = Project.objects.filter(id=prjID)
	if (detail == 'projecttype'):
		projectDetail = projectRaw[0].get_projecttype_display()
	elif (detail == 'projectStatus'):
		projectDetail = projectRaw[0].get_projectStatus_display()
	elif (detail == 'boothStatus'):
		projectDetail = projectRaw[0].get_boothStatus_display()
	elif (detail == 'workshopStatus'):
		projectDetail = projectRaw[0].get_workshopStatus_display()
	elif (detail == 'talkStatus'):
		projectDetail = projectRaw[0].get_talkStatus_display()
	elif (detail == 'serviceTalk'):
		projectDetail = projectRaw[0].get_serviceTalk_display()
	elif (detail == 'serviceWorkshop'):
		projectDetail = projectRaw[0].get_serviceWorkshop_display()
	elif (detail == 'serviceDistribution'):
		projectDetail = projectRaw[0].get_serviceDistribution_display()
	elif (detail == 'serviceStatus'):
		projectDetail = projectRaw[0].get_serviceStatus_display()
	elif (detail == 'boothPowerPlan'):
		projectDetail = projectRaw[0].get_boothPowerPlan_display()
	elif (detail == 'boothPower'):
		projectDetail = projectRaw[0].get_boothPower_display()
	elif (detail == 'boothHallStatus'):
		projectDetail = projectRaw[0].get_boothHallStatus_display()
	elif (detail == 'boothArea'):
		projectDetail = projectRaw[0].get_boothArea_display()
	elif (detail == 'language'):
		projectDetail = projectRaw[0].get_language_display()
	elif (detail == 'generalEvent'):
		projectDetail = projectRaw[0].get_generalEvent_display()
	elif (detail == 'projectArea'):
		projectDetail = projectRaw[0].get_projectArea_display()
	elif (detail == 'boothExtras'):
		projectDetail = projectRaw[0].get_boothExtras_display()
	
	return projectDetail

def get_project_ids(id, detail):
        cursor = connections['default'].cursor()
        if (detail == 'name'):
            cursor.execute('SELECT "projectName" FROM project_project WHERE owner_id = %s', [id])
        elif (detail == 'id'):
            cursor.execute('SELECT id FROM project_project WHERE owner_id = %s', [id])
        row = cursor.fetchall()
        if row:
            project_ids = row

        return project_ids

def get_project_id(id):
	cursor = connections['default'].cursor()
	cursor.execute('SELECT id from project_project WHERE "projectName" like %s', [id])
	row = cursor.fetchone()
	if row:
		project_id = row

	return str(project_id[0])


def create_pretalx_user(attributes):
	logger.info('#### - User Sync ####')
	check_uid = ''.join(attributes['uid'])
	check_name = "{} {}".format(''.join(attributes['givenName']), ''.join(attributes['sn']))
	check_nick = ''.join(attributes['givenName'])
	if 'member' in attributes:
		if ''.join(attributes['member']) == u't':
			check_admin = '1'
		else:
			check_admin = '0'
	else:
		check_admin = '0'
	if 'dc' in attributes:
		if ''.join(attributes['dc']) == u't':
			check_staff = '1'
		else:
			check_staff = '0'
	else:
		check_staff = '0'
	CODE_CHARSET = list('ABCDEFGHJKLMNPQRSTUVWXYZ3789')
	code = get_random_string(length=6, allowed_chars=CODE_CHARSET)
	cursor = connections['pretalx'].cursor()
	cursor.execute("SELECT * FROM person_user WHERE email = %s LIMIT 1", [check_uid])
	row = cursor.fetchone()
	if row:
			logger.info('Updating user in pretalx')
			cursor.execute("UPDATE person_user SET is_administrator = %s WHERE email = %s", [check_admin, check_uid])
			#cursor.execute("SELECT id FROM person_user WHERE email = %s LIMIT 1", [check_uid])
			#row = cursor.fetchone()
			#if not row:
			#		logger.info('Adding user to team')
			#		temp_uid = row[0]
			#		logger.info('temp_uid: %s', temp_uid)
			#		cursor.execute("INSERT INTO event_team_members (team_id, user_id) VALUES ('2', %s)", [temp_uid])
			#		cursor.execute("INSERT INTO event_team_members (team_id, user_id) VALUES ('3', %s)", [temp_uid])
	else:
			logger.info('Creating user in pretalx')
			cursor.execute("INSERT INTO person_user (email, password, name, nick, is_active, is_staff, is_superuser, locale, timezone, get_gravatar, is_administrator, code) VALUES (%s, '', %s, %s, '1', %s, '0', 'en', 'UTC', '1', %s, %s)", [check_uid, check_name, check_nick, check_staff, check_admin, code])
			#logger.info('Adding user to team')
			#cursor.execute("SELECT id FROM person_user WHERE email = %s LIMIT 1", [check_uid])
			#row = cursor.fetchone()
			#temp_uid = row[0]
			#cursor.execute("INSERT INTO event_team_members (team_id, user_id) VALUES ('2', %s)", [temp_uid])
			#cursor.execute("INSERT INTO event_team_members (team_id, user_id) VALUES ('3', %s)", [temp_uid])
	return True


def get_submissions(user, event):
	result = {}
	submissions = []
	if event == 'talk':
		endpoint = 'https://submission.make-munich.de/api/events/mm19-talks/speakers/?q={user_id}'
	elif event == 'workshop':
		endpoint = 'https://submission.make-munich.de/api/events/mm19-workshops/speakers/?q={user_id}'
	url = endpoint.format(user_id=user)
	headers = {'Authorization': 'Token ' + settings.PRETALX_API}
	response = requests.get(url, headers=headers)
	result = response.json()
	resSet = result.get('results')
	for key in resSet:
  		if key['submissions']:
  				for id in key['submissions']:
  					details = get_submission_details(id, event)
  					submissions.append(details)

	return submissions


def get_submission_details(id, event):
	result = {}
	submission_details = []
	if event == 'talk':
		endpoint = 'https://submission.make-munich.de/api/events/mm19-talks/submissions/{submission_id}'
	elif event == 'workshop':
		endpoint = 'https://submission.make-munich.de/api/events/mm19-workshops/submissions/{submission_id}'
	url = endpoint.format(submission_id=id)
	headers = {'Authorization': 'Token ' + settings.PRETALX_API}
	response = requests.get(url, headers=headers)
	result = response.json()
	submission_details.append(result['code'])
	submission_details.append(result['title'])

	return submission_details
