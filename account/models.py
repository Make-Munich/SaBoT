from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
import logging
from djangosaml2.signals import pre_user_save
from sabot.utils import create_pretalx_user

logger = logging.getLogger('djangosaml2')


class UserProfile(models.Model):
	user = models.OneToOneField(User, related_name="legacy_profile")
	authToken = models.CharField(max_length=64, null=True)
	is_orga = models.BooleanField(default=False)
	is_planning = models.BooleanField(default=False)
	is_checkin = models.BooleanField(default=False)

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
	if created:
		UserProfile.objects.update_or_create(user=instance)

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
	instance.legacy_profile.save()

@receiver(pre_user_save, sender=User)
def pretalx_user(sender, instance, attributes, user_modified, **kwargs):
	logger.info('Attributes: %s' % attributes)
	result = create_pretalx_user(attributes)
	logger.info('Result: %s' % result)