from django.db import models
from django import forms
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from multiselectfield import MultiSelectField
from django_currentuser.db.models import CurrentUserField
from upload_validator import FileTypeValidator
from phonenumber_field.modelfields import PhoneNumberField

PSTATE = (
	(1, _("project_state_submitted")),
	(2, _("project_state_accepted")),
	(3, _("project_state_rejected")),
	(4, _("project_state_cancelled")),
	(5, _("project_state_waitinglist")),
)

PTYPE = (
	(1, _("project_type_maker_nosale")),
	(2, _("project_type_maker_sale")),
	(3, _("project_type_startup")),
	(4, _("project_type_professional")),
)

CLANG = (
	(1, _("lang_german")),
	(2, _("lang_english")),
)

EVENT = (
	(1, "Make Munich 2019"),
	(2, "Make Munich 2020"),
)


AREAS = (
	(1, _("area_3d")),
	(2, _("area_arduino")),
	(3, _("area_art")),
	(4, _("area_biohacking")),
	(5, _("area_bionics")),
	(6, _("area_crafts")),
	(7, _("area_design")),
	(8, _("area_fabrication")),
	(9, _("area_education")),
	(10, _("area_electronics")),
	(11, _("area_fashion")),
	(12, _("area_food")),
	(13, _("area_garden")),
	(14, _("area_healthcare")),
	(15, _("area_home")),
	(16, _("area_interaction")),
	(17, _("area_iot")),
	(18, _("area_model")),
	(19, _("area_newmaterials")),
	(20, _("area_rpi")),
	(21, _("area_robot")),
	(22, _("area_science")),
	(23, _("area_social")),
	(24, _("area_sustainability")),
	(25, _("area_startup")),
	(26, _("area_transportation")),
	(27, _("area_wearables")),
	(28, _("area_wellness")),
	(29, _("area_vr")),
	(30, _("area_young")),
	(31, _("area_houses")),
	(32, _("area_other")),
)


BAREA = (
	(0, _("booth_area_inprogress")),
	(1, _("booth_area_3d")),
	(2, _("booth_area_vr")),
	(3, _("booth_area_biohacking")),
	(4, _("booth_area_creativity")),
	(5, _("booth_area_design")),
	(6, _("booth_area_diy")),
	(7, _("booth_area_electronic")),
	(8, _("booth_area_fablab")),
	(9, _("booth_area_fashtech")),
	(10, _("booth_area_foodmaker")),
	(11, _("booth_area_radio")),
	(12, _("booth_area_green")),
	(13, _("booth_area_robotics")),
	(14, _("booth_area_tools")),
	(15, _("booth_area_workshops")),
	(16, _("booth_area_kids")),
	(17, _("booth_area_education")),
	(18, _("booth_area_catering")),
	(19, _("booth_area_startup")),
	(20, _("booth_area_mobility")),
	(21, _("booth_area_own")),
	(22, _("booth_area_iot")),
)

BSTATE = (
	(1, _("booth_state_submitted")),
	(2, _("booth_state_accepted")),
	(3, _("booth_state_rejected")),
	(4, _("booth_state_detailing")),
	(5, _("booth_state_completed")),
	(6, _("booth_state_nobooth")),
)

BHSTATE = (
	(1, _("booth_hall_state_discrepancy")),
	(2, _("booth_hall_state_plan")),
)

CHAIRS = (
	(0, "0"),
	(1, "1"),
	(2, "2"),
	(3, "3"),
	(4, "4"),
	(5, "5"),
	(6, "6"),
	(7, "7")
)

TABLES = (
	(0, "0"),
	(1, "1"),
	(2, "2"),
	(3, "3"),
	(4, "4"),
	(5, "5"),
	(6, "6"),
	(7, "7"),
	(8, "8"),
	(9, "9"),
	(10, "10"),
)

BENCHES = (
	(0, "0"),
	(1, "1"),
	(2, "2"),
	(3, "3"),
	(4, "4"),
	(5, "5"),
	(6, "6"),
	(7, "7"),
	(8, "8"),
)

POWER = (
	(1, _("power_no")),
	(2, _("power_5a")),
	(3, _("power_10a")),
	(4, _("power_16a")),
)

EXTRAS = (
	(1, _("extra_more_space")),
	(2, _("extra_more_chairs_tables")),
	(3, _("extra_no_tables")),
	(4, _("extra_backwall")),
	(5, _("extra_outdoor")),
	(6, _("extra_internet")),
	(7, _("extra_ac")),
	(8, _("extra_corner_booth")),
	(9, _("extra_radio")),
)


SLEVEL = (
	(1, _("sponsor_maker")),
	(2, _("sponsor_make_sell")),
	(3, _("sponsor_professional")),
	(4, _("sponsor_platin")),
	(5, _("sponsor_gold")),
	(6, _("sponsor_silver")),
	(7, _("sponsor_bronze")),
	(8, _("sponsor_starter")),
	(9, _("sponsor_supporter")),
)

ABRANDING = (
	(1, _("area_brand_no")),
	(2, _("area_brand_3d")),
	(3, _("area_brand_vr")),
	(4, _("area_brand_biohacking")),
	(5, _("area_brand_creativity")),
	(6, _("area_brand_design")),
	(7, _("area_brand_diy")),
	(8, _("area_brand_electronic")),
	(9, _("area_brand_fablab")),
	(10, _("area_brand_Fashtech")),
	(11, _("area_brand_foodmaker")),
	(12, _("area_brand_radio")),
	(13, _("area_maker_green")),
	(14, _("area_brand_robotic")),
	(15, _("area_brand_tools")),   
)

TALK = (
	(1, _("talk_none")),
	(2, _("talk_1")),
	(3, _("talk_2")),      
)

WORKSHOP = (
	(1, _("workshop_none")),
	(2, _("workshop_agreed")),
	(3, _("workshop_booth")),
	(4, _("workshop_area")),
	(5, _("workshop_both")),     
)

TICKETS = (
	(0, "0"),
	(1, "1"),
	(2, "2"),
	(3, "3"),
	(4, "4"),
	(5, "5"),
	(6, "6"),
	(7, "7"),
	(8, "8"),
	(9, "9"),
	(10, "10"),
	(11, "11"),
	(12, "12"),
	(13, "13"),
	(14, "14"),
	(15, "15"),
	(16, "16"),
	(17, "17"),
	(18, "18"),
	(19, "19"),
	(20, "20"),
)

PARKING = (
	(0, "0"),
	(1, "1"),
	(2, "2"),
	(3, "3"),
	(4, "4"),
	(5, "5"),
)

TSTATE = (
	(1, " "),
	(1, _("ticket_state_progress")),
	(2, _("ticket_state_final")),      
)

DISTRIBUTION = (
	(1, _("ticket_distribution_checkin")),
	(2, _("ticket_distribution_mail")),
	(3, _("ticket_distribution_send")),
)

TASTATE = (
	(1, _("talk_state_submitted")),
	(2, _("talk_state_accepted")),
	(3, _("talk_state_rejected")),
	(4, _("talk_state_detailing")),
	(5, _("talk_state_completed")),
	(6, _("talk_state_notalk")),
)

WOSTATE = (
	(1, _("workshop_state_submitted")),
	(2, _("workshop_state_accepted")),
	(3, _("workshop_state_rejected")),
	(4, _("workshop_state_detailing")),
	(5, _("workshop_state_completed")),
	(6, _("workshop_state_noworkshop")),
)


class Project(models.Model):
	projectName = models.CharField(max_length=128, unique=True, verbose_name=_("label_general_name"))
	projectStatus = models.PositiveIntegerField(choices=PSTATE, verbose_name=_("label_general_status"), default=1)
	createDate = models.DateField(auto_now_add=True,editable=False)
	modifyDate = models.DateField(auto_now=True, editable=False)
	modifyBy = CurrentUserField()
	firstname = models.CharField(max_length=30, blank=False, verbose_name=_("label_general_firstname"))
	lastname = models.CharField(max_length=30, blank=False, verbose_name=_("label_general_lastname"))
	email = models.EmailField(blank=False, verbose_name=_("label_general_email"))
	phone = PhoneNumberField(blank=False, verbose_name=_("label_general_phone"))
	homepage = models.URLField(blank=True, verbose_name=_("label_general_homepage"))
	projecttype = models.PositiveIntegerField(choices=PTYPE, verbose_name=_("label_general_type"), default=1)
	language = models.PositiveIntegerField(choices=CLANG, verbose_name=_("label_general_language"), default=1)
	hear = models.CharField(max_length=50, blank=True, verbose_name=_("label_general_hear"))
	recommendation = models.CharField(max_length=200, blank=True, verbose_name=_("label_general_recommendations"))
	generalComment = models.TextField(blank=True, verbose_name=_("label_general_comments"))
	internalComment = models.TextField(blank=True, verbose_name=_("label_general_internal"))
	generalContact = models.CharField(max_length=50, blank=True, verbose_name=_("label_general_contact"))
	generalEvent = models.PositiveIntegerField(choices=EVENT, verbose_name=_("label_general_event"), default=1)


	descriptionDE = models.TextField(max_length=900, blank=True, verbose_name=_("label_project_desc_german"))
	descriptionEN = models.TextField(max_length=900, blank=True, verbose_name=_("label_project_desc_english"))
	projectArea = MultiSelectField(blank=True, choices=AREAS, max_choices=7, verbose_name=_("label_project_categories"))
	logoOrg = models.ImageField(blank=True, upload_to="projects/logos", verbose_name=_("label_project_logoorg"), validators=[FileTypeValidator(allowed_types=[ 'image/jpeg','image/png'])])
	logoProject1 = models.ImageField(blank=True, upload_to="projects/pictures", verbose_name=_("label_project_logo_1"), validators=[FileTypeValidator(allowed_types=[ 'image/jpeg','image/png'])])
	logoProject2 = models.ImageField(blank=True, upload_to="projects/pictures", verbose_name=_("label_project_logo_2"), validators=[FileTypeValidator(allowed_types=[ 'image/jpeg','image/png'])])
	logoProject3 = models.ImageField(blank=True, upload_to="projects/pictures", verbose_name=_("label_project_logo_3"), validators=[FileTypeValidator(allowed_types=[ 'image/jpeg','image/png'])])
	logoProject4 = models.ImageField(blank=True, upload_to="projects/pictures", verbose_name=_("label_project_logo_4"), validators=[FileTypeValidator(allowed_types=[ 'image/jpeg','image/png'])])
	logoProject5 = models.ImageField(blank=True, upload_to="projects/pictures", verbose_name=_("label_project_logo_5"), validators=[FileTypeValidator(allowed_types=[ 'image/jpeg','image/png'])])
	logoTeam = models.ImageField(blank=True, upload_to="projects/teams", verbose_name=_("label_project_logoteam"), validators=[FileTypeValidator(allowed_types=[ 'image/jpeg','image/png'])])
	video = models.URLField(blank=True, verbose_name=_("label_project_video"))
	videoFile = models.FileField(blank=True, upload_to="projects/videos", verbose_name=_("label_project_video_file"), validators=[FileTypeValidator(allowed_types=[ 'video/mp4','video/quicktime', 'video/x-msvideo', 'video/x-ms-wmv'])])


	boothDescription = models.TextField(max_length=300, blank=True, verbose_name=_("label_booth_description"))		
	boothLocation = models.CharField(max_length=10, blank=True, verbose_name=_("label_booth_location"))
	boothArea = models.PositiveIntegerField(choices=BAREA, verbose_name=_("label_booth_area"), default=0)
	boothStatus = models.PositiveIntegerField(choices=BSTATE, verbose_name=_("label_booth_status"), default=6)
	boothHallStatus = models.PositiveIntegerField(choices=BHSTATE, verbose_name=_("label_booth_hall"), default=1)
	boothTables = models.PositiveIntegerField(choices=TABLES, verbose_name=_("label_booth_tables_req"), default=1)
	boothChairs = models.PositiveIntegerField(choices=CHAIRS, verbose_name=_("label_booth_chairs_req"), default=1)
	boothBenches = models.PositiveIntegerField(choices=BENCHES, verbose_name=_("label_booth_benches_req"), default=0)
	boothTablesPlan = models.PositiveIntegerField(default=0, blank=True, verbose_name=_("label_booth_tables_plan"))
	boothTablesRes = models.PositiveIntegerField(default=0, blank=True, verbose_name=_("label_booth_tables_res"))
	boothChairsPlan = models.PositiveIntegerField(default=0, blank=True, verbose_name=_("label_booth_chairs_plan"))
	boothChairsRes = models.PositiveIntegerField(default=0, blank=True, verbose_name=_("label_booth_chairs_res"))
	boothBenchesPlan = models.PositiveIntegerField(default=0, blank=True, verbose_name=_("label_booth_benches_plan"))
	boothBenchesRes = models.PositiveIntegerField(default=0, blank=True, verbose_name=_("label_booth_benches_res"))
	boothSize = models.CharField(max_length=20, blank=True, verbose_name=_("label_booth_size"))
	boothSizePlan = models.CharField(max_length=20, blank=True, verbose_name=_("label_booth_size_plan"))
	boothPower = models.PositiveIntegerField(choices=POWER, verbose_name=_("label_booth_power"), default=1)
	boothPowerPlan = models.PositiveIntegerField(choices=POWER, verbose_name=_("label_booth_power_plan"), default=1)
	boothExtras = MultiSelectField(blank=True, choices=EXTRAS, max_choices=9, verbose_name=_("label_booth_extras"))
	boothExtrasComment = models.TextField(max_length=300, blank=True, verbose_name=_("label_booth_extras_comments"))
	boothOwn = models.TextField(max_length=300, blank=True, verbose_name=_("label_booth_own"))
	boothSafety= models.TextField(max_length=300, blank=True, verbose_name=_("label_booth_safety"))
	boothComment = models.TextField(blank=True, verbose_name=_("label_booth_internal"))
	boothLastMinute = models.TextField(blank=True, verbose_name=_("label_booth_lastminute"))


	serviceTickets = models.PositiveIntegerField(choices=TICKETS, verbose_name=_("label_service_tickets"), default=0)
	serviceTicketsGiven = models.PositiveIntegerField(choices=TICKETS, verbose_name=_("label_service_tickets_dist"), default=0)
	serviceParking = models.PositiveIntegerField(choices=PARKING, verbose_name=_("label_service_parking"), default=0)
	serviceParkingGiven = models.PositiveIntegerField(choices=PARKING, verbose_name=_("label_service_parking_dist"), default=0)
	serviceStatus = models.PositiveIntegerField(choices=TSTATE, verbose_name=_("label_service_ticketstatus"), default=0)
	serviceDistribution = models.PositiveIntegerField(choices=DISTRIBUTION, verbose_name=_("label_service_distribution"), default=1)
	serviceDistributed = models.CharField(max_length=30, blank=True, verbose_name=_("label_service_distributed"))
	serviceSponsorlevel = models.CharField(max_length=30, blank=True, verbose_name=_("label_service_sponsorlevel"))
	serviceAreabranding = models.CharField(max_length=30, blank=True, verbose_name=_("label_service_areabranding"))
	serviceWorkshop = models.PositiveIntegerField(choices=WORKSHOP, verbose_name=_("label_service_workshop"), default=1)
	serviceTalk = models.PositiveIntegerField(choices=TALK, verbose_name=_("label_service_talk"), default=1)
	serviceComments = models.TextField(max_length=300, blank=True, verbose_name=_("label_service_agreements"))
	serviceInternalComments = models.TextField(max_length=300, blank=True, verbose_name=_("label_service_internal"))


	talkComment = models.TextField(blank=True, verbose_name=_("label_talk_comment"))
	talkStatus = models.PositiveIntegerField(choices=TASTATE, verbose_name=_("label_talk_status"), default=6)

	workshopComment = models.TextField(blank=True, verbose_name=_("label_workshop_comment"))
	workshopStatus = models.PositiveIntegerField(choices=WOSTATE, verbose_name=_("label_workshop_status"), default=6)

	owner = models.ForeignKey(User,editable=False,related_name="projects")
	participants = models.ManyToManyField(User,blank=True,editable=False,related_name="projectparticipation", through="ProjectParticipants")
	accepted = models.BooleanField(default=False, editable=False)
	year = models.PositiveIntegerField(editable=False, verbose_name=_("label_project_year"))

	def has_read_permission(self, user):
		return ProjectParticipants.objects.filter(user=user).count() > 0 or user == self.owner

	def has_write_permission(self, user):
		return ProjectParticipants.objects.filter(user=user,isAdmin=True).count() > 0 or user == self.owner

class ProjectParticipants(models.Model):
	project = models.ForeignKey(Project)
	user = models.ForeignKey(User)
	isAdmin = models.BooleanField(default=False)
