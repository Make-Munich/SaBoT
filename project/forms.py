from django import forms
from models import Project
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, Submit, Div, HTML, ButtonHolder
from crispy_forms.bootstrap import FormActions, StrictButton, TabHolder, Tab, PrependedText, InlineCheckboxes, InlineField, Accordion, AccordionGroup
import requests
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from django_currentuser.middleware import (
    get_current_user, get_current_authenticated_user)
from sabot.utils import get_user_details, get_submissions, get_project_ids
from phonenumber_field.widgets import PhoneNumberPrefixWidget
import logging

logger = logging.getLogger('djangosaml2')

class ProjectGeneralForm(forms.ModelForm):
	class Meta:
		model = Project
		fields = (
			"projectName",
			"firstname",
			"lastname",
			"email",
			"phone",
			"homepage",
			"projecttype",
			"language",
			"hear",
			"recommendation",
			"generalComment"
			)

		widgets = {
			"phone": PhoneNumberPrefixWidget
		}

	def __init__(self, *args, **kwargs):
		super(ProjectGeneralForm, self).__init__(*args, **kwargs)
		current_user = get_current_user()
		userFirstName = get_user_details(current_user, 'firstname')
		userLastName = get_user_details(current_user, 'lastname')
		userEmail = get_user_details(current_user, 'email')
		userIsStaff = get_user_details(current_user, 'isstaff')
		self.fields['firstname'].initial = userFirstName
		self.fields['lastname'].initial = userLastName
		self.fields['email'].initial = userEmail
		self.helper = FormHelper()
		self.helper.form_class = 'auto_submit_form'
#		self.helper.form_class='form-horizontal'
		self.helper.layout = Layout(
			Field("projectName"),
			Div(
        		Div('firstname', css_class='col-md-6',),
        		Div('lastname', css_class='col-md-6',),
        		css_class='row',
    		),
			Field("email"),
			Field("phone"),
			Field("homepage"),
			Field("projecttype"),
			Field("language"),
			Field("hear"),
			Div(HTML(_("form_text_general_recommendation"))),
			Field("recommendation"),
			Field("generalComment"),
#			FormActions(Submit("Save", "Save changes"))
			)
#		if userIsStaff:
#			self.helper.layout.append(Div(HTML("Admin only")))

		#self.helper[1:3].wrap_together(Div, css_class="name-wrapper")
		#self.helper['firstname'].wrap(Field, css_class="col-md-6", wrapper_class="firstname")
		#self.helper['lastname'].wrap(Field, css_class="col-md-6", wrapper_class="lastname")

		if self.instance is not None and self.instance.id is not None:
			self.helper.add_input(Submit(_("form_label_save"), _("form_label_save_changes")))
		else:
			self.helper.add_input(Submit(_("form_label_save"), _("form_label_register")))



class ProjectDescriptionForm(forms.ModelForm):
	class Meta:
		model = Project
		fields = (
			"descriptionDE",
			"descriptionEN",
			"projectArea",
			"logoOrg",
			"logoProject1",
			"logoProject2",
			"logoProject3",
			"logoProject4",
			"logoProject5",
			"logoTeam",
			"video",
			"videoFile"
			)

		widgets = {
			"logoOrg" : forms.widgets.FileInput,
			"logoProject1" : forms.widgets.FileInput,
			"logoProject2" : forms.widgets.FileInput,
			"logoProject3" : forms.widgets.FileInput,
			"logoProject4" : forms.widgets.FileInput,
			"logoProject5" : forms.widgets.FileInput,
			"logoTeam" : forms.widgets.FileInput,
			"videoFile" : forms.widgets.FileInput,
		}

	def __init__(self, *args, **kwargs):
		super(ProjectDescriptionForm, self).__init__(*args, **kwargs)
		self.helper = FormHelper()
		self.helper.form_class = 'auto_submit_form'
		self.helper.layout = Layout(
			Div(HTML(_("form_text_project_description"))),
			Field("descriptionDE"),
			Field("descriptionEN"),
			Div(HTML(_("form_text_project_cat"))),
			Field("projectArea"),
			Div(HTML(_("form_text_project_logo"))),
		)
		if self.instance and self.instance.logoOrg:
			self.helper.layout.extend([
				Field("logoOrg"),
				Div(HTML("<p>Current image:</p><img src=\"{{object.logoOrg.url}}\" style=\"max-height:200px\"/>"), css_class = "control-group"),
				#<a href=\"{% url 'project_remove_logoOrg' %}\">Remove</a>
			])
		else:
			self.helper.layout.append(
				Div(Div(Field("logoOrg"),css_class = "col-md-2"), css_class = "row"),
			)

		if self.instance and self.instance.logoProject1:
			self.helper.layout.extend([
				Field("logoProject1"),
				Div(HTML("<p>Current image:</p><img src=\"{{object.logoProject1.url}}\" style=\"max-height:200px\"/>"), css_class = "control-group"),
			])
		else:
			self.helper.layout.append(
				Div(Div(Field("logoProject1"),css_class = "col-md-2"), css_class = "row"),
			)

		if self.instance and self.instance.logoProject2:
			self.helper.layout.extend([
				Field("logoProject2"),
				Div(HTML("<p>Current image:</p><img src=\"{{object.logoProject2.url}}\" style=\"max-height:200px\"/>"), css_class = "control-group"),
			])
		else:
			self.helper.layout.append(
				Div(Div(Field("logoProject2"),css_class = "col-md-2"), css_class = "row"),
			)

		if self.instance and self.instance.logoProject3:
			self.helper.layout.extend([
				Field("logoProject3"),
				Div(HTML("<p>Current image:</p><img src=\"{{object.logoProject3.url}}\" style=\"max-height:200px\"/>"), css_class = "control-group"),
			])
		else:
			self.helper.layout.append(
				Div(Div(Field("logoProject3"),css_class = "col-md-2"), css_class = "row"),
			)

		if self.instance and self.instance.logoProject4:
			self.helper.layout.extend([
				Field("logoProject4"),
				Div(HTML("<p>Current image:</p><img src=\"{{object.logoProject4.url}}\" style=\"max-height:200px\"/>"), css_class = "control-group"),
			])
		else:
			self.helper.layout.append(
				Div(Div(Field("logoProject4"),css_class = "col-md-2"), css_class = "row"),
			)

		if self.instance and self.instance.logoProject5:
			self.helper.layout.extend([
				Field("logoProject5"),
				Div(HTML("<p>Current image:</p><img src=\"{{object.logoProject5.url}}\" style=\"max-height:200px\"/>"), css_class = "control-group"),
			])
		else:
			self.helper.layout.append(
				Div(Div(Field("logoProject5"),css_class = "col-md-2"), css_class = "row"),
			)

		if self.instance and self.instance.logoTeam:
			self.helper.layout.extend([
				Field("logoTeam"),
				Div(HTML("<p>Current image:</p><img src=\"{{object.logoTeam.url}}\" style=\"max-height:200px\"/>"), css_class = "control-group"),
			])
		else:
			self.helper.layout.append(
				Div(Div(Field("logoTeam"),css_class = "col-md-2"), css_class = "row"),
			)

		if self.instance and self.instance.videoFile:
  			self.helper.layout.extend([
				Field("videoFile"),
				Div(HTML("<p>Current video:</p><a href=\"{{object.videoFile.url}}\">{{object.videoFile.name}}</a></p>"), css_class = "control-group"),
			])
		else:
			self.helper.layout.append(
				Div(Div(Field("videoFile"),css_class = "col-md-2"), css_class = "row"),
			)

		self.helper.layout.append(Field("video")),
		self.helper.add_input(Submit(_("form_label_save"), _("form_label_save_changes"))),


class ProjectBoothForm(forms.ModelForm):
	class Meta:
		model = Project
		fields = (
			"boothDescription",
			"boothTables",
			"boothChairs",
			"boothBenches",
			"boothPower",
			"boothExtras",
			"boothExtrasComment",
			"boothOwn",
			"boothSafety",
                        "boothLocation"
			)

	def __init__(self, *args, **kwargs):
		super(ProjectBoothForm, self).__init__(*args, **kwargs)
		self.helper = FormHelper()
		self.helper.form_class = 'auto_submit_form'
		self.helper.layout = Layout(
			Field("boothLocation", readonly=True),
                        Div(HTML(_("form_text_booth_intro"))),
			Div(HTML(_("form_text_booth_description"))),
			Field("boothDescription"),
			Div(HTML(_("form_text_booth_info"))),
			Div(
        		Div('boothTables', css_class='col-md-4',),
        		Div('boothChairs', css_class='col-md-4',),
				Div('boothBenches', css_class='col-md-4',),
        		css_class='row',
    		),
			Field("boothPower"),
			Field("boothExtras"),
			Field("boothExtrasComment"),
			Field("boothOwn"),
			Field("boothSafety"),
#			FormActions(Submit("Save", "Save changes"))
		)
		self.helper.add_input(Submit(_("form_label_save"), _("form_label_save_changes")))


class ProjectServiceForm(forms.ModelForm):
	class Meta:
		model = Project
		fields = (
			"serviceTickets",
			"serviceParking",
			)

	def __init__(self, *args, **kwargs):
		super(ProjectServiceForm, self).__init__(*args, **kwargs)
		self.helper = FormHelper()
		self.helper.form_class = 'auto_submit_form'
		self.helper.layout = Layout(
			Div(HTML(_("form_text_service_info"))),
			Div(HTML(_("form_text_service_team_size"))),
			Field("serviceTickets"),
			Div(HTML(_("form_text_service_parking"))),
			Div(HTML(_("form_text_service_parking_num"))),
			Field("serviceParking"),
#			FormActions(Submit("Save", "Save changes"))
		)
		self.helper.add_input(Submit(_("form_label_save"), _("form_label_save_changes")))

class ProjectTalkForm(forms.ModelForm):
	class Meta:
		model = Project
		fields = ("talkComment"),

	def __init__(self, *args, **kwargs):
		super(ProjectTalkForm, self).__init__(*args, **kwargs)
		current_user = get_current_user()
		userEmail = get_user_details(current_user, 'email')
            #userID = get_user_details(current_user, 'id')
		#prj_ids = get_project_ids(userID, 'id')
        #        if prj_ids:
        #            userTalks = get_submissions(userEmail, 'talk')
                #prj_name = self.fields['projectName'].to_python()
                #logger.info(prj_name)
                #userTalks = get_submissions(userEmail, 'talk')
		self.helper = FormHelper()
		self.helper.form_class = 'auto_submit_form'
		self.helper.layout = Layout(
#			ButtonHolder(
#				HTML(_("<a class='btn btn-primary'href='https://submission.make-munich.de/mm19/me/submissions'>View or add submissions</a>")),
#			),
			Div(HTML(_("form_text_talk_info"))),
			StrictButton(_("form_text_talk_button"), css_class="btn-default", onclick="pretalxWindow = window.open('http://submission.make-munich.de/mm19-talks/me/submissions', '_blank')"),
		)
		#for talk in userTalks:
  		#	uTalk = ' '.join(talk)
  		#	self.helper.layout.append(
		#			Div(HTML(uTalk))
		#		)

		#self.helper.layout.append(
		#	Div(HTML(userTalks))
		#)

        #        self.helper.layout.append(
        #                Div(HTML(prj_ids))
        #        )

		self.helper.layout.append(
			Field("talkComment"),
		)
			#Div(HTML(userTalks)),
#			FormActions(Submit("Save", "Save changes"))
		self.helper.add_input(Submit(_("form_label_save"), _("form_label_save_changes")))

class ProjectWorkshopForm(forms.ModelForm):
	class Meta:
		model = Project
		fields = ("workshopComment",)

	def __init__(self, *args, **kwargs):
		super(ProjectWorkshopForm, self).__init__(*args, **kwargs)
		current_user = get_current_user()
		userEmail = get_user_details(current_user, 'email')
		#userWorkshops = get_submissions(userEmail, 'workshop')
		self.helper = FormHelper()
		self.helper.form_class = 'auto_submit_form'
		self.helper.layout = Layout(
#			ButtonHolder(
#				HTML(_("<a class='btn btn-primary'href='https://submission.make-munich.de/mm19w/me/submissions'>View or add submissions</a>")),
#			),
			Div(HTML(_("form_text_workshop_info"))),
			StrictButton(_("form_text_workshop_button"), css_class="btn-default", onclick="pretalxWindow = window.open('http://submission.make-munich.de/mm19-workshops/me/submissions', '_blank')"),
			#StrictButton(_("form_text_workshop_button"), css_class="btn-default disabled"),
		)
		#for workshop in userWorkshops:
  		#	uWorkshop = ' '.join(workshop)
  		#	self.helper.layout.append(
		#			Div(HTML(uWorkshop))
		#		)

		#self.helper.layout.append(
		#	Div(HTML(userWorkshops))
		#)

		self.helper.layout.append(
			Field("workshopComment"),
		)
			#Div(HTML(userWorkshops)),
#			FormActions(Submit("Save", "Save changes"))
		self.helper.add_input(Submit(_("form_label_save"), _("form_label_save_changes")))

class ProjectInternalForm(forms.ModelForm):
	class Meta:
		model = Project
		fields = (
			"projectStatus",
			"internalComment",
			"generalContact",
			"generalEvent",
			"boothLocation",
			"boothArea",
			"boothStatus",
			"boothHallStatus",
			"boothTablesPlan",
			"boothTablesRes",
			"boothChairsPlan",
			"boothChairsRes",
			"boothBenchesPlan",
			"boothBenchesRes",
			"boothSize",
			"boothSizePlan",
			"boothPowerPlan",
			"boothComment",
			"boothLastMinute",
			"serviceTicketsGiven",
			"serviceParkingGiven",
			"serviceStatus",
			"serviceDistribution",
			"serviceDistributed",
			"serviceSponsorlevel",
			"serviceAreabranding",
			"serviceWorkshop",
			"serviceTalk",
			"serviceComments",
			"serviceInternalComments",
			"talkStatus",
			"workshopStatus",

			)

	def __init__(self, *args, **kwargs):
		super(ProjectInternalForm, self).__init__(*args, **kwargs)
		self.helper = FormHelper()
		self.helper.form_class = 'auto_submit_form'
		self.helper.layout = Layout(
			Accordion(
    			AccordionGroup(_("text_link_general"),
        			Div(
        				Div('projectStatus', css_class='col-md-6',),
        				Div('generalEvent', css_class='col-md-6',),
        				css_class='row',
					),
					Field("generalContact"),
					Field("internalComment"),
    			),
    			AccordionGroup(_("text_link_project"),

   				),
				AccordionGroup(_("text_link_service"),
					Div(
						Div('serviceTicketsGiven', css_class='col-md-6',),
						Div('serviceParkingGiven', css_class='col-md-6',),
						css_class='row',
					),
					Field("serviceStatus"),
					Field("serviceDistribution"),
					Field("serviceDistributed"),
					Field("serviceSponsorlevel"),
					Field("serviceAreabranding"),
					Div(
						Div('serviceWorkshop', css_class='col-md-6',),
						Div('serviceTalk', css_class='col-md-6',),
						css_class='row',
					),
					Field("serviceComments"),
					Field("serviceInternalComments"),
    			),
				AccordionGroup(_("text_link_booth"),
        			Field("boothLocation"),
					Field("boothArea"),
					Field("boothStatus"),
					Field("boothHallStatus"),
					Div(
						Div('boothTablesPlan', css_class='col-md-6',),
						Div('boothTablesRes', css_class='col-md-6',),
						css_class='row',
					),
					Div(
						Div('boothChairsPlan', css_class='col-md-6',),
						Div('boothChairsRes', css_class='col-md-6',),
						css_class='row',
					),
					Div(
						Div('boothBenchesPlan', css_class='col-md-6',),
						Div('boothBenchesRes', css_class='col-md-6',),
						css_class='row',
					),
					Div(
						Div('boothSize', css_class='col-md-6',),
						Div('boothSizePlan', css_class='col-md-6',),
						css_class='row',
					),
					Field("boothPowerPlan"),
					Field("boothComment"),
					Field("boothLastMinute"),
    			),
				AccordionGroup(_("text_link_talk"),
					Field("talkStatus"),

    			),
				AccordionGroup(_("text_link_workshop"),
					Field("workshopStatus"),

    			),
			),
		)
		self.helper.add_input(Submit(_("form_label_save"), _("form_label_save_changes")))

